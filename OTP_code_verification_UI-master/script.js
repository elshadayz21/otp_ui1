/** @format */

const inputs = document.querySelectorAll("input");
button = document.querySelector("button");
// console.log(inputs, button);

// to iterate and focus on the input field to focus on the different boxes based on the input inserted 
inputs.forEach((input, index1) => {
  input.addEventListener("keyup", (e) => {
    //indexes of the boxes
    const currentInput = input,
      nextInput = input.nextElementSibling,
      prevInput = input.previousElementSibling;

    if (currentInput.value.length > 1) {
        // to clear it if there is more than one value inserted in the box
      currentInput.value = currentInput.value.slice(0,1);
      return;
    }
    //to remove the disabled attribute after a value is inserted int the current input field
    if (nextInput && nextInput.hasAttribute("disabled") && currentInput !== "") 
    {
      nextInput.removeAttribute("disabled");
      nextInput.focus();
    }
    //on backspace press to go back
    if (e.key === "Backspace") {
        //iterate over all inputs again
      inputs.forEach((input, index2) => {
        // if the inde1 of the current input is less than or equal to the index2 of the input in the outer loop
        // and the previous element exists, set the disabled attribute on the input and focus on the current element  
        if (index1 <= index2 && prevInput) {
          input.setAttribute("disabled", true);
          currentInput.value = "";
          prevInput.focus();
        }
      });
    }
    //if the iteration is in the last box activate the input button
    if (!inputs[3].disabled && !inputs[3].value !== "") {
      button.classList.add("active");
    //   button.removeAttribute('disabled')
      return;
    }
    button.classList.remove("active");
  });
});

//for the cursor to focus on the first boc when the window is reloaded 
window.addEventListener("load", () => inputs[0].focus());
